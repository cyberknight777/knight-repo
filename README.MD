# Knight-Repo

## A Repo For Termux/Debian-Based Users

> Add kNIGHT's Repository with this command : 

| Method    | Command                                                     |
|:----------|:------------------------------------------------------------|
| **curl**  | `bash -c "$(curl -fsSL https://bit.do/knight-repo)"`  |


> Packages Available:-
+ [Botamsg](https://gitlab.com/cyberknight777/botamsg)
+ [Mkdeb](https://gitlab.com/cyberknight777/mkdeb)
+ [Bashify](https://gitlab.com/cyberknight777/bashify)
+ [qcacld-inj](https://gitlab.com/cyberknight777/knight-repo)
## How To Use:-


> About Package :
```bash
For Packages Install :
Just Type These Command :
$ apt install <pkg>

For Packages Update :
Update Repository using :
$ apt update
$ apt upgrade <pkg>
```
